function define_path_type(tag, timestamp, record)
    -- Split 'orig_path' for all
    new_rec = record
    new_rec["path"] = record["orig_path"]
    new_rec["path"] = new_rec["path"]:gsub("//", "/")
    new_rec["orig_path"] = nil

    -- process 'path' for creating 'repository' value
    subst_workrnd = string.gsub(new_rec["path"], "/", " ")
    parts = {}
    for str in string.gmatch(subst_workrnd, '%S+') do
        table.insert(parts, str)
    end

    -- Identify path types
    if (parts[1] == "almalinux")
    then
        new_rec["path_type"] = "alma"

    elseif (parts[1] == "cdn.redhat.com")
    then
        new_rec["path_type"] = "cdn"

    elseif (parts[1] == "centos")
    then
        new_rec["path_type"] = "centos"

    elseif (parts[1] == "centos-stream")
    then
        new_rec["path_type"] = "stream"

    elseif (parts[1] == "cern")
    then
        new_rec["path_type"] = "cern"

    elseif (parts[1] == "enterprise")
    then
        new_rec["path_type"] = "enterprise"

    elseif (parts[1] == "epel")
    then
        new_rec["path_type"] = "epel"

    elseif (parts[1] == "internal" and parts[2] == "yumsnapshot")
    then
        new_rec["path_type"] = "internyum"

    elseif (parts[1] == "mirror")
    then
        new_rec["path_type"] = "mirror"

    elseif (parts[1] == "rockylinux")
    then
        new_rec["path_type"] = "rocky"

    elseif (parts[1] == "scientific")
    then
        new_rec["path_type"] = "scientific"

    else
        print("Unknown type!")
    end

    return 2, timestamp, new_rec
end



function process_dump1(tag, timestamp, record)
    -- Split 'dump' value and create 'service' (1st part)
    print(record["path_type"])
    new_rec = record
    parts1 = {}
    for str1 in string.gmatch(new_rec["dump"], "%S+") do
        table.insert(parts1, str1)
    end

    new_rec["part1"] = parts1[0]
    new_rec["service"] = parts1[1]

    -- Split 2nd part of 'dump' value
    -- Create 'service2' with 1st part and 'version2' with 2nd part.
    if(new_rec["path_type"] == "centos" or new_rec["path_type"] == "cern" or new_rec["path_type"] == "mirror" or new_rec["path_type"] == "cdn")
    then
        parts2 = {}
        subst_workrnd = string.gsub(parts[2], "/", " ")
        for str2 in string.gmatch(subst_workrnd, '%S+') do
            table.insert(parts2, str2)
        end
        new_rec["service2"] = parts2[1]
        new_rec["version2"] = parts2[2]
    end

    -- Create 'trailer' key:value
    if #parts1 == 2
    -- Covers: 'centos' and 'cern'
    then
        new_rec["trailer"] = "\n"
    elseif #parts1 == 3
    -- Covers: 'mirror' and 'cdn'
    then
        new_rec["trailer"] = parts1[3]
    else
        -- Covers: 'stream', 'epel', 'almalinux' and 'internal'
        res = ""
        prefinal = #parts1 - 1
        for i=2,prefinal do
            print(parts1[i])
            res = res .. parts1[i] .. " "
        end
        res = res .. parts1[#parts1]
        new_rec["trailer"] = res
    end

    return 2, timestamp, new_rec
end


function process_program(tag, timestamp, record)
    -- Fix 'program' for all
    new_rec = record
    subst_workrnd = string.gsub(new_rec["program"], "%[.+%]", " ")
    parts = {}
    for str in string.gmatch(subst_workrnd, '%S+') do
        table.insert(parts, str)
    end

    -- print(#parts)
    -- print(parts[1])
    new_rec["program"] = parts[1]
    new_rec["pid"] = string.match(new_rec["program"], "%[(%d+)%]")

    return 2, timestamp, new_rec
end


function process_time(tag, timestamp, record)
    -- Extract 'timezone' for all
    new_rec = record
    parts = {}
    for str in string.gmatch(new_rec["time"], '%S+') do
        table.insert(parts, str)
    end

    -- print(#parts)
    -- print(parts[1])
    -- print(parts[2])
    new_rec["timezone"] = parts[2]

    return 2, timestamp, new_rec
end



function process_path(tag, timestamp, record)
    -- Fix 'path' for all
    new_rec = record
    new_rec["path"] = record["orig_path"]
    new_rec["path"] = new_rec["path"]:gsub("//", "/")
    new_rec["orig_path"] = nil

    -- process 'path' for creating 'repository'value
    subst_workrnd = string.gsub(new_rec["path"], "/", " ")
    parts = {}
    for str in string.gmatch(subst_workrnd, '%S+') do
        table.insert(parts, str)
    end
    res = "/"
    repository_length = #parts - 3
    for i=1,repository_length do
        -- print(parts[i])
        res = res .. parts[i] .. "/"
    end
    last_part = #parts - 2
    res = res .. parts[last_part]
    new_rec["repository"] = res

    -- retrieve part of path to be added as 2nd value in 'trailer' key.
    if(new_rec["path_type"] == "internalyumsnapshot" or new_rec["path_type"] == "cdn")
    then
        trailer2 = "/" .. parts[#parts - 3] .. "/" .. parts[#parts - 2] .. "/" .. parts[#parts - 1] .. "/" .. parts[#parts]
        trailer1 = new_rec["trailer"]
        trailers = {trailer1, trailer2}
        new_rec["trailer"] = trailers
    end

    return 2, timestamp, new_rec
end



function process_cern(tag, timestamp, record)
    new_rec = record
    if (new_rec["path_type"] == "cernplus")
    then
        print(new_rec["path_type"])
    
        parts = {}
        subst_workrnd = string.gsub(new_rec["path"], "/", " ")
        parts = {}
        for str in string.gmatch(subst_workrnd, '%S+') do
            table.insert(parts, str)
        end
        -- print(#parts)
        -- print(parts[2])
        -- print(parts[3])
        -- print(parts[4])
        -- print(parts[5])

        --Just for reference on the naming in logstash.config
        -- what = parts[2]
        -- version = parts[3] -- arch is not always at this part (arch is called version in logstash.config :facepalm:)
        -- x1 = parts[4]
        -- x2 = parts[5]

        -- Checking all /cern/slcXX paths
        if(string.find(parts[2], "^slc"))
        -- /cern/slc6X/x86_64/yum/extras/repoview/zfs.html
        -- /cern/slc6X/updates/x86_64/SRPMS/ipa-3.0.0-37.el6.src.rpm
        -- /cern/slc55/updates/x86_64/RPMS/libsmbclient-devel-3.0.33-3.29.el5_5.1.x86_64.rpm
        -- I need to find at least one '/cern/slcZZ', where it endswith '/repodata/repomd.xml' to compare
        then
            print("/cern/slc")
            new_rec["os_flavor"] = string.lower(parts[2])
            os_version = string.sub(parts[2], 4, 5)
            new_rec["os_arch"] = parts[3]
            new_rec["os_tag"] = new_rec["os_flavor"] .. os_version
            new_rec["os_version"] = os_version .. ".X"

        -- Checking all /cern/centos paths
        elseif (string.find(parts[2], "^centos"))
        then
            print("/cern/centos")
            if (parts[3] == "6" and parts[4] == "sclo")
            then
                parts[2] = "slc"
            elseif(parts[3] == "7")
            then
                parts[2] = "cc"
            else
                parts[2] = "c"
            end
        
            new_rec["os_flavor"] = string.lower(parts[2])
            os_version = parts[3]
            new_rec["os_arch"] = parts[5]
            new_rec["os_tag"] = new_rec["os_flavor"] .. os_version
            new_rec["os_version"] = os_version .. ".X"

        elseif (string.find(parts[2], "[alma|rhel]"))
        then
            print("/cern/[alma|rhel]")    
            new_rec["os_flavor"] = string.lower(parts[2])
            os_version = parts[3]
            new_rec["os_arch"] = parts[5]
            new_rec["os_tag"] = new_rec["os_flavor"] .. os_version
            new_rec["os_version"] = os_version .. ".X"
        end
    end

    new_rec["acctype"] = "live"

    return 2, timestamp, new_rec
end



function process_cdn(tag, timestamp, record)
    new_rec = record
    if (new_rec["path_type"] == "cdn")
    then
        print(new_rec["path_type"])
    
        parts = {}
        subst_workrnd = string.gsub(new_rec["path"], "/", " ")
        parts = {}
        for str in string.gmatch(subst_workrnd, '%S+') do
            table.insert(parts, str)
        end
        -- print(#parts)
        -- print(parts[4])     -- flavor
        -- print(parts[6])     -- os_version
        -- print(parts[7])     -- x1
        -- print(parts[8])     -- os_arch
        -- print(parts[9])     -- trailer      Not used
    
        if (parts[4] ~= "rhel")
        then
            parts[8] = parts[7]
        end

        new_rec["os_flavor"] = "rhel"
        os_version = parts[6]
        new_rec["os_arch"] = parts[8]
        new_rec["os_tag"] = new_rec["os_flavor"] .. os_version
        new_rec["os_version"] = os_version .. ".X"
    end

    new_rec["acctype"] = "live"

    return 2, timestamp, new_rec
end



function process_internyum(tag, timestamp, record)
    new_rec = record
    if (new_rec["path_type"] == "internalyumsnapshot")
    then
        print(new_rec["path_type"])
    
        parts = {}
        subst_workrnd = string.gsub(new_rec["path"], "/", " ")
        parts = {}
        for str in string.gmatch(subst_workrnd, '%S+') do
            table.insert(parts, str)
        end
        -- print(#parts)
        -- print(parts[3])     -- x0
        -- print(parts[4])     -- x1
        -- print(parts[5])     -- x2
        -- print(parts[6])     -- x3
        -- print(parts[7])     -- x4
        -- print(parts[8])     -- x5

        if (parts[4] == "cern")
        -- /internal/yumsnapshot/20220707/cern/centos/7/extras/x86_64/repodata/repomd.xml
        then
            if (string.find(parts[5], "centos"))
            then
                if (parts[6] == "7")
                then
                    parts[5] = "cc"
                else
                    parts[5] = "c"
                end
                if (parts[7] == "sclo" and parts[6] == "6")
                then
                    new_rec["os_flavor"] = "slc"
                    os_version = parts[6]
                    new_rec["os_arch"] = parts[8]
                    new_rec["os_tag"] = new_rec["os_flavor"] .. os_version
                    new_rec["os_version"] = os_version .. ".X"
                else
                    new_rec["os_flavor"] = string.lower(parts[5])
                    os_version = parts[6]
                    new_rec["os_arch"] = parts[8]
                    new_rec["os_tag"] = new_rec["os_flavor"] .. os_version
                    new_rec["os_version"] = os_version .. ".X"
                end
            end

            elseif (string.find(parts[5], "[alma|rhel]"))
            then
                new_rec["os_flavor"] = string.lower(parts[5])
                os_version = parts[6]
                new_rec["os_arch"] = parts[8]
                new_rec["os_tag"] = new_rec["os_flavor"] .. os_version
                new_rec["os_version"] = os_version .. ".X"
            else
                -- The logstash equivalent holds the following: (lines 209 - 213)
                -- grok {
                --     match => {
                --       "x2" => "slc%{USERNAME:os_version}"
                --     }
                -- }
                -- But it doesn't use this x2 in the end!
                new_rec["os_flavor"] = "slc"
                new_rec["os_arch"] = parts[6]
            end                            

        elseif (parts[4] == "cdn.redhat.com")
        then
            if (parts[8] == "server")
            then
                -- similar to the previous non-translated grok
                -- grok {
                --     match => {
                --       "trailer" => "/%{USERNAME:os_version}/%{USERNAME:d0}/%{USERNAME:os_arch}"
                --     }
                -- }
                new_rec["os_flavor"] = "rhel"
            else
                -- similar to the previous non-translated grok
                -- grok {
                --     match => {
                --       "trailer" => "/%{USERNAME:os_arch}/"
                --     }
                -- }
                new_rec["os_flavor"] = "rhel"
                new_rec["os_version"] = parts[8] .. ".X"
            end
        end

    new_rec["acctype"] = "snap"

    return 2, timestamp, new_rec
end



function process_misc(tag, timestamp, record)
    new_rec = record
    if (new_rec["path_type"] == "centos" or new_rec["path_type"] == "stream" or new_rec["path_type"] == "epel" or new_rec["path_type"] == "almalinux" or new_rec["path_type"] == "rocky" or new_rec["path_type"] == "scientific")
    then
        print(new_rec["path_type"])

        parts = {}
        subst_workrnd = string.gsub(new_rec["path"], "/", " ")
        for str in string.gmatch(subst_workrnd, '%S+') do
            table.insert(parts, str)
        end
        -- print(#parts)
        -- print(parts[2])     -- os_flavor
        -- print(parts[3])     -- os_version

        new_rec["os_flavor"] = string.lower(parts[2])
        os_version = string.gsub(parts[3], "-stream$", "")
        new_rec["os_tag"] = new_rec["os_flavor"] .. os_version
        new_rec["os_version"] = os_version .. ".X"

    end

    new_rec["acctype"] = "live"

    return 2, timestamp, new_rec
end



function process_mirror(tag, timestamp, record)
    new_rec = record
    if (new_rec["path_type"] == "mirror")
    then
        tags = {"mirror"}
        new_rec["tags"] = tags
    end

    return 2, timestamp, new_rec
end


function process_enterprise(tag, timestamp, record)
    print("TODO")

end
