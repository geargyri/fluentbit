Usage instructions

Testing mode
- Clone this repository to a VM (e.g. inside /root/ directory)
- Install fluentbit. The executable is in /opt/fluent-bit/bin
- $ cd /opt/fluent-bit/bin
- $ ./fluent-bit -c /root/fluentbit/fb_mainconf.conf	# executing fluentbit manually. Define only the filepath of the main configuration file.
- The main parser is defined inside the main conf file. It's hard-coded filepath will be /root/fluentbit/fb_parser.conf
- All lua functions are defined inside the same lua file. This file is placed in the same dir as the main conf file. (e.g. /root/fleuntbit/fb_lua_processing.lua)
